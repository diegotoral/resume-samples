module Palsio
  module Pdf
    extend self

    # The default method used to render the template into HTML for further
    # conversion into PDF.
    def default_html_renderer
      ApplicationController.public_method :render
    end

    # Default method used to generate the PDF data.
    def default_pdf_renderer
      WickedPdf.new.public_method :pdf_from_string
    end

    # Renders the specified template/view name into a PDF file.
    # Returns a temporary file that can latter be uploaded or processed.
    #
    # template - template name to render
    # filename - the filename
    # options - hash containing extra options
    #   layout - HTML layout to load when rendering the template
    #   html_renderer - something that renders the template into a string
    #   pdf_renderer - something that renders an HTML string into PDF
    #   locals  - variables to pass down when rendering the view/template
    #   other options are passed down to pdf_renderer
    def render_to_pdf(template, filename, options={})
      options = options.dup
      layout = options.delete(:layout)
      locals = options.delete(:locals) || {}
      html_renderer = options.delete(:html_renderer) || default_html_renderer
      pdf_renderer = options.delete(:pdf_renderer) || default_pdf_renderer

      html = html_renderer.call(
        layout: layout,
        template: template,
        locals: locals,
      )

      options.deep_transform_keys! { |k| k.to_sym }
      pdf_renderer.call(html, options.merge(return_file: true))
    end
  end
end
