require 'rails_helper'

RSpec.describe Palsio::Pdf do
  let(:html_renderer) {  double(:renderer) }
  let(:pdf_renderer) { double(:pdf_renderer) }

  before do
    allow(html_renderer).to receive(:call)
    allow(pdf_renderer).to receive(:call)
  end

  describe '#default_html_renderer' do
    it 'returns a callable' do
      expect(subject.default_html_renderer).to respond_to :call
    end
  end

  describe '#default_pdf_renderer' do
    it 'returns a callable' do
      expect(subject.default_pdf_renderer).to respond_to :call
    end
  end

  describe '#render_to_pdf' do
    it 'renders the template with the specified options' do
      expect(html_renderer).to receive(:call).with(
        layout: 'layouts/pdf.html',
        template: 'test.pdf',
        locals: {}
      )

      subject.render_to_pdf(
        'test.pdf',
        'test',
        layout: 'layouts/pdf.html',
        locals: {},
        html_renderer: html_renderer,
        pdf_renderer: pdf_renderer
      )
    end

    it 'renders the pdf with the specified options and rendered template' do
      html = '<h1>Hello, World!</h1>'

      allow(html_renderer).to receive(:call) { html }

      expect(pdf_renderer).to receive(:call).with(
        html,
        pdf_option: 'some-option',
        other_option: 'other-option',
        return_file: true
      )

      subject.render_to_pdf(
        'test.pdf',
        'test',
        layout: 'layouts/pdf.html',
        locals: {},
        html_renderer: html_renderer,
        pdf_renderer: pdf_renderer,
        pdf_option: 'some-option',
        other_option: 'other-option'
      )
    end
  end
end
