require 'rails_helper'

RSpec.describe Champion, type: :model do
  subject(:champion) { build(:champion) }

  let(:campaign1)  { create(:campaign) }
  let(:campaign2)  { create(:campaign) }
  let(:campaign3)  { create(:campaign) }
  let(:params) { {  stripeToken: 'abc',
                    shipping_street: "121 King West",
                    shipping: {country:"CA"},
                    shipping_zip: "M1K3J0",
                    shipping_city: "Toronto",
                    shipping_tel:  "11122233",
                    use_billing_address: "1",
                  }
                }

  let(:perk)  { create(:perk, title: 'sticker', campaign: campaign1, donation_amount: Money.new(1000, 'USD')) }
  let(:perk1) { create(:perk, title: 'sticker1', campaign: campaign2, donation_amount: Money.new(1000, 'USD')) }
  let(:perk2)  { create(:perk, title: 'sticker', campaign: campaign1, donation_amount: Money.new(1000, 'USD')) }

  let(:champion1)  { create(:champion) }
  let(:champion2)  { create(:champion) }

  let(:charity1)   { create(:charity) }
  let(:charity2)   { create(:charity) }

  let(:user1)  { create(:user) }
  let(:user2)  { create(:user) }
  let(:donation1) { PerkDonation.create_for(perk, user1, params, :finished) }
  let(:donation2) { PerkDonation.create_for(perk, user1, params, :finished) }
  let(:donation3) { PerkDonation.create_for(perk1, user2, params, :finished) }
  let(:donation4) { PerkDonation.create_for(perk1, user2, params, :finished) }
  let(:donation5) { PerkDonation.create_for(perk2, user2, params, :finished) }
  let(:donation6) { PerkDonation.create_for(perk1, user2, params, :finished) }

  context "multiple campaigns, multiple champions" do
    before do
      campaign1.champions << [champion1, champion2]
      campaign2.champions << [champion1, champion2]
      campaign1.charities << [charity1, charity2]
      campaign2.charities << [charity1, charity2]
      perk.champions << [champion1, champion2]
      perk1.champions << [champion1, champion2]
      perk2.champions << [champion2]
      donation1
      donation2
      donation3
      donation4
    end

    describe "#total_raised" do
      it 'returns raised amount of its associated perks' do
        donation5
        expect(champion1.total_raised).to eql(4000)
        expect(champion2.total_raised).to eql(5000)
      end

      context 'when associated to a donation_request' do
        it 'sums up the donation_request donations where source is the champion' do
          donation_request = create(:donation_request, user: champion1)
          create(:general_donation, donation_request: donation_request, source: champion1, amount: 900)
          create(:general_donation, donation_request: donation_request, source: champion1, amount: 400)
          create(:general_donation, donation_request: donation_request, source: champion2, amount: 300)

          expect(champion1.total_raised).to eq 134000
        end
      end
    end

    describe "#total_charities" do
      it 'returns count of charities associated to champion' do
        champion1.charities << charity1

        expect(champion1.total_charities).to eql(1)
      end
    end

    describe "#total_contributors" do
      it 'returns total contributors for donations from perks associated to champion' do
        expect(champion1.total_contributors).to eql(2)
      end
    end

    describe "#leaderboard" do
      it "selects donors for the campaign associated with champion in sort order" do
        donation5
        donation6

        expect(champion1.leaderboard.map { |u| [u, u.total_donated_cents] }).
          to eq [[user2, 3000], [user1, 2000]]

        expect(champion2.leaderboard.map { |u| [u, u.total_donated_cents] }).
          to eq [[user2, 4000], [user1, 2000]]
      end
    end
  end

  describe '#total_charities' do
    it 'counts the number of charities' do
      subject.charities = []

      expect(subject.total_charities).to eq 0
    end
  end

  describe '#total_perks' do
    it 'counts the number of perks' do
      subject.perks = []

      expect(subject.total_perks).to eq 0
    end
  end

  describe '#total_perks_contributions' do
    it 'delegates call to ContributionsCountQuery' do
      query = double(:query, results: [])

      expect(ContributionsCountQuery).to receive(:new).with(subject.perks) { query }

      subject.total_perks_contributions
    end
  end
end
