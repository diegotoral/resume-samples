require 'rails_helper'

RSpec.describe Champion, type: :model do
  subject(:champion) { build(:champion) }

  describe '#total_raised' do
    let(:perk_1) { create(:perk) }
    let(:perk_2) { create(:perk) }
    let(:perk_3) { create(:perk) }

    before do
      # Add champion to perk
      create(:perk_champion, perk: perk_1, champion: champion)
      create(:perk_champion, perk: perk_2, champion: champion)
      create(:perk_champion, perk: perk_3, champion: champion)

      # Create donations
      create(:perk_donation, perk: perk_1, amount_cents: 1_000)
      create(:perk_donation, perk: perk_2, amount_cents: 1_000)
    end

    it 'returns the total amount raised by the champion' do
      expect(champion.total_raised).to eq 2_000
    end

    it 'includes the total amount raised by donation request where source is the champion' do
      donation_request = create(:donation_request, user: champion)

      create(
        :general_donation,
        donation_request: donation_request,
        source: champion,
        amount_cents: 1_000
      )

      expect(champion.total_raised).to eq 3_000
    end
  end

  describe '#leaderboard' do
    it 'delegates call to UsersTotalDonatedQuery' do
      query = double(:query, results: [])

      expect(UsersTotalDonatedQuery).to receive(:new).with(champion.perk_donations) { query }

      champion.leaderboard
    end
  end

  describe '#total_perks' do
    it 'returns the number of perks associated with this champion' do
      create(:perk_champion, champion: champion)

      expect(champion.total_perks).to eq 1
    end
  end

  describe "#total_charities" do
    it 'returns the number of charities associated to this champion' do
      champion.charities = []

      expect(champion.total_charities).to eq 0
    end
  end

  describe "#total_contributors" do
    it 'returns total contributors for donations from perks associated to champion' do
      perk = create(:perk)

      create(:perk_champion, perk: perk, champion: champion)

      create(:perk_donation, perk: perk)
      create(:perk_donation, perk: perk)

      expect(champion.total_contributors).to eq 2
    end
  end

  describe '#fundraising_supporters' do
    it 'returns all users that contributed to this champion somehow' do
      perk = create(:perk)
      user_1 = create(:user)
      user_2 = create(:user)
      user_3 = create(:user)

      create(:perk_champion, perk: perk, champion: champion)

      create(:perk_donation, perk: perk, user: user_1)
      create(:general_donation, source: champion, user: user_2)

      supporters = champion.fundraising_supporters

      expect(supporters).not_to include(user_3)
      expect(supporters).to include(user_1, user_2)
    end
  end
end
