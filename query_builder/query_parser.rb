class QueryParser
  class Text
    attr_reader :value

    def initialize(value)
      @value = value
    end
  end

  class Scope
    attr_reader :value

    def initialize(value)
      @value = value
    end
  end

  class Field
    attr_reader :value

    def initialize(value)
      @value = value
    end
  end

  class Query
    attr_writer :field
    attr_reader :texts, :scopes

    def initialize
      @texts = []
      @scopes = []
      @field = nil
    end

    def execute
      "User.#{scoped}.where(#{where_clause})"
    end

    private

    def term
      @texts.map(&:value).join(' ')
    end

    def scoped
      @scopes.map(&:value).join('.')
    end

    def where_clause
      "#{@field.value}: #{term}"
    end
  end

  def initialize(tokens)
    @tokens = tokens
    @query = Query.new
  end

  def parse
    @tokens.each do |token, value|
      send("parse_#{token}", value)
    end

    @query
  end

  private

  def parse_text(value)
    @query.texts << Text.new(value)
  end

  def parse_is(value)
    @query.scopes << Scope.new(value)
  end

  def parse_in(value)
    @query.field = Field.new(value)
  end
end

