class QueryScanner
  def self.scan(query)
    new(query).scan
  end

  def initialize(query)
    @scanner = StringScanner.new(query)
  end

  def scan
    tokens = []
    tokens << scan_token until @scanner.eos?
    tokens
  end

  private

  def scan_token
    scan_is || scan_in || scan_text
  end

  def scan_is
    if @scanner.scan(/is:/)
      value = @scanner.scan_until(/\w+/)
      scan_space
      [:is, value]
    end
  end

  def scan_in
    if @scanner.scan(/in:/)
      value = @scanner.scan_until(/\w+/)
      scan_space
      [:in, value]
    end
  end

  def scan_space
    @scanner.scan(/\s+/)
  end

  def scan_text
    if value = @scanner.scan_until(/\w+/)
      scan_space
      [:text, value]
    end
  end
end
