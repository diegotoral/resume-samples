class DataImporter
  IMPORTERS = {
    'DESCRIÇÃO' => DescriptionImporter,
    'LICENCIAMENTO' => LicensingImporter,
    'CONSUMO_ÁGUA' => WaterConsumptionImporter,
    'GESTAO RH' => WaterResourcesManagementImporter,
    'LEILAO' => AuctionImporter,
    'BNDES' => BndesImporter,
  }

  def initialize(data_import_id)
    @data_import_id = data_import_id
  end

  def import
    clean_power_plants

    spreadsheet.each_with_pagename do |name, sheet|
      importer = IMPORTERS[name]
      importer(sheet).import if importer.present?
    end
  end

  private

  def clean_power_plants
    PowerPlant.destroy_all
  end

  def sheet
    @sheet ||= Roo::Excelx.new(data_import.file.download)
  end

  def data_import
    @data_import ||= DataImport.find(@data_import_id)
  end
end
