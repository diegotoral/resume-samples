class BndesImporter
  FIRST_ROW_INDEX = 2

  POWER_PLANT_COLUMNS = {
    name: 1,
    aneel: 2,
  }

  SHEET_COLUMNS = {
    bndes_funded: 3,
    client: 4,
    project_description: 5,
    hiring_date: 6,
    contracted_value: 7,
    financial_cost: 8,
    interest: 9,
    term_contract_deficiency: 10,
    term_contract_amortization: 11,
    support_form: 12
  }

  def initialize(sheet)
    @sheet = sheet
  end

  def import!
    (FIRST_ROW_INDEX..(@sheet.last_row - 1)).each do |current_row|
      process_line(current_row)
    end
  end

  private

  def cell(row, col)
    @sheet.cell(row, col)
  end

  def extract_params_from(columns, row)
    columns.each_with_object({}) { |name, index, p| p[name] = cell(row, index) }
  end

  def process_line(row_id)
    power_plant = find_or_create_power_plant(power_plant_params)
    update_power_plant_from_row(power_plant, row)
  end

  def find_or_create_power_plant(row)
    params = extract_params_from(POWER_PLANT_COLUMNS, row)

    PowerPlant.find_or_create_by(params)
  end

  def update_power_plant_from_row(power_plant, row)
    params = extract_params_from(SHEET_COLUMNS, row)

    params[:hiring_date] = parse_date(params[:hiring_date])
    params[:bndes_funded] = parse_bndes_funded_status(params[:bndes_funded])

    power_plant.update(params)
  end

  def parse_bndes_funded_status(status)
    case status
    when 'Não foi financiada pelo BNDES'
      return false
    when 'Financiada pelo BNDES'
      return true
    else
      return nil
    end
  end

  def parse_date(date)
    return date if date.is_a? Date
  end
end
