class ChargeTransactionJob < ApplicationJob
  sidekiq_options queue: :payments, retry: 3

  def perform(transaction_id, provider, options)
    gateway = Palsio::Gateways.from_provider(provider)
    transaction = CreditCardTransaction.find transaction_id

    charge_transaction.call(gateway: gateway, transaction: transaction)
  end

  attr_writer :charge_transaction

  private

  def charge_transaction
    @charge_transaction ||= Transactions::ChargeService.build
  end
end
