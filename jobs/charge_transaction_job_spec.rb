require 'rails_helper'

RSpec.describe ChargeTransactionJob, type: :job do
  subject(:job) { described_class.new }

  it { is_expected.to be_retryable 3 }
  it { is_expected.to be_processed_in :payments }

  describe '#perform' do
    it 'delegates processing to a service' do
      transaction = create(:credit_card_transaction)
      charge_transaction = double(:charge_transaction)
      job.charge_transaction = charge_transaction

      expect(charge_transaction).to receive(:call)

      job.perform(transaction.id, :stripe, {})
    end
  end
end
