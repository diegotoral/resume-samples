# Samples

This repository contains some sample code extracted from numerous projects I had opportunity to contribute to.
Here you'll find only parts of my work as most part of it was extracted from private or protected codebases.

## Important to note

- Do not share samples here
- Some samples are 2 or even 3 years old
- Specs not included
- It's not garanteed to run
- Some samples were slightly changed to hide any business logic and private data

# Contact information

Diego Toral <diegotoral@gmail.com>
github.com/diegotoral | linkedin.com/in/diegotoral
